<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'pauline' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'pauline' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+}tj(Gl^WuPS@(66Ke}-SqLZ6[4pt>MCD7?@1iY6F/;b,vNMK^ Gf_/^p7K|wz${' );
define( 'SECURE_AUTH_KEY',  'UCu{)Yfv0`D=mWSX3s}T8g:dh+b^.zJO&/3aF]>i:_S3QWS^N6Xy>B- A<a@f1*q' );
define( 'LOGGED_IN_KEY',    ']rE9cdUrD22rZT,E.XG3+hwh5EY]^Sse!fJ8zF|/}o@=L(!F!:b8*iRe314J4vs3' );
define( 'NONCE_KEY',        '{w91$]?~t)vxt]~;)jnIe-!@jYfm 3<(7zLV=1vy(K;0G(KYUD)W!9W^Ejqm4tIY' );
define( 'AUTH_SALT',        'S6_63Rq%:&4;q?N{;~ph *DHN-.]O2_v[43`NN)}?hVjOt3?qGRzA%&(`23XP9]H' );
define( 'SECURE_AUTH_SALT', 'l]L)2?#J@AJN@6Bjkphoa&OF2(CMJOqMa<fr0;dw 3vWDW0Ap>W1]uL<`=|km<xU' );
define( 'LOGGED_IN_SALT',   'zj8_FJ5.b3XTexd~{U-3Ra,:Rkt-$(HLXXPx~Os#9=$;YMg9>}I.nEN=*0{Y=h0m' );
define( 'NONCE_SALT',       '-dnYy{4+4%wg~:+/>%w8?*zP7)xxIzG)UL9Y(~/u&k+/d/PJxJU0tHKmFB qG!2/' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
